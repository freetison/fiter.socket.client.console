﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using fiter.abstractions.Models.Access;
using fiter.abstractions.Models.Sockect;
using fiter.socket.client.console.Models;
using fiter.socket.client.console.Services.Interfaces;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using Serilog;
// ReSharper disable StringLiteralTypo

namespace fiter.socket.client.console
{
    class Program
    {
        private const int RECONNECTION_TIME = 3;

        private static IConfigurationRoot _configuration;
        static SocketSettings _socketSettings;
        static readonly string Host = Dns.GetHostName();
        private static Stopwatch _stopwatch;
        private static HubConnection _signalRConnection;

        public static void Main(string[] args)
        {
            _stopwatch = Stopwatch.StartNew();

            string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            _configuration = builder.Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(_configuration)
                .CreateLogger();

            var serviceCollection = new ServiceCollection();
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            _socketSettings = new SocketSettings();
            _configuration.GetSection("SocketSettings").Bind(_socketSettings);


            _signalRConnection = new HubConnectionBuilder()
            .WithUrl($"{_socketSettings.Host}{_socketSettings.Hub}?host={Host}", options =>
            {
                options.Transports = HttpTransportType.WebSockets;
                options.Headers = new Dictionary<string, string>() { { "app-key", _socketSettings.AppKey }, { "cat-key", _socketSettings.CatKey } };
            })
            .AddMessagePackProtocol()
            .ConfigureLogging(logging => {
                logging.SetMinimumLevel(LogLevel.Critical);
                logging.AddConsole();
            })
            .Build();

            _signalRConnection.ServerTimeout = TimeSpan.FromMinutes(1);

            _signalRConnection.On<IdentifyInfo>("Connected", OnConnected);

            _signalRConnection.On<BasicNotifyInfo>("AccessNotify", OnAccessNotify);

            _signalRConnection.On<string>("Notify", (data) => { Log.Information($"{data}"); });

            _signalRConnection.On<string>("DirectMessage", (data) => { Log.Information($"{data}"); });

            _signalRConnection.On<string>("Echo", (data) =>
            {
                Log.Information($"{data}");
                _signalRConnection.InvokeAsync("Echo", "pong", cancellationToken: new CancellationTokenSource().Token);
            });

            OpenSignalRConnection(Host);

            Console.ReadLine();
        }


        private static async void OpenSignalRConnection(string host)
        {
            var pauseBetweenFailures = TimeSpan.FromSeconds(RECONNECTION_TIME);
            var retryPolicy = Policy
                .Handle<Exception>()
                .WaitAndRetryForeverAsync(i => pauseBetweenFailures, (exception, timeSpan) => { Log.Error(exception.Message); });

            await retryPolicy.ExecuteAsync(async () =>
            {
                Log.Information($"Trying to connect to SignalR server from host: {host}");
                await TryOpenSignalRConnection();
            });
        }

        private static async Task<bool> TryOpenSignalRConnection()
        {
            Log.Information("Starting SignalR connection");
            _signalRConnection.Closed += SignalRConnection_Closed;
            await _signalRConnection.StartAsync();
            Log.Information("SignalR connection established");
            return true;
        }

        private static async Task SignalRConnection_Closed(Exception arg)
        {
            _stopwatch.Stop();
            Log.Information($"SignalR connection is closed after {_stopwatch.Elapsed:dd\\:hh\\:mm\\:ss\\.fff}");
            await _signalRConnection.StopAsync();
            _signalRConnection.Closed -= SignalRConnection_Closed;
            OpenSignalRConnection(Host);
        }


        // ----------------------------------------------
        private static void OnConnected(IdentifyInfo data) => Log.Information($"Identification Info received : Name :{data.Name} -- Group: {data.Group} ");
        private static void OnAccessNotify(BasicNotifyInfo data) => Log.Information($"Identification Info received : {JsonConvert.SerializeObject(data)} ");
    }
}
