﻿using fiter.socket.client.console.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace fiter.socket.client.console
{
    public class Startup
    {
        IConfigurationRoot Configuration { get; }

        public Startup()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // App Configuration
            services.AddSingleton(Configuration);
            services.Configure<SocketSettings>(Configuration.GetSection(nameof(SocketSettings)));

          
            //var baseUrl = Configuration.GetValue<string>("ApiSettings:ApiUrl");
            //services.AddHttpClient<IMailService, MailService>(client =>
            //{
            //    client.BaseAddress = new Uri($"{baseUrl}/api/v1/NotifyByEmail");

            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    client.DefaultRequestHeaders.Add("User-Agent", "d-fens HttpClient");
            //});

        }
    }
}
