﻿namespace fiter.socket.client.console.Models
{
    public class SocketSettings
    {
        public string Host { get; set; }
        public string Api { get; set; }
        public string Hub { get; set; }
        public string AppKey { get; set; }
        public string CatKey { get; set; }
    }
}
