﻿namespace fiter.socket.client.console.Models.Interfaces
{
    public interface IDeviceInfo
    {
        int? IdDevice { get; set; }
        int? IdSucursal { get; set; }
    }
}
