﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace fiter.socket.client.console.Services.Interfaces
{
    public interface IVendingClientHub
    {
        Task<HubConnection> WebSocketConnectionBuilderAsync(string socketUrl);
    }
}