﻿using System.Threading.Tasks;

namespace fiter.socket.client.console.Services.Interfaces
{
    /// <summary>
    /// Defines the events associated with the Hub.
    /// </summary>
    public interface IBasicHub
    {
        
        /// <summary>
        /// This event occurs when a value is posted to the AccessController.
        /// </summary>
        Task Notify(string message);

        /// <summary>
        /// Send a message to specific client.
        /// </summary>
        Task DirectMessage(string pattern, string message);

        /// <summary>
        /// Ping to service
        /// </summary>
        /// <returns></returns>
        Task Echo(string ping);
    }
}
