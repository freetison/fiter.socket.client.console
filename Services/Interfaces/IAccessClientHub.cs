﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace fiter.socket.client.console.Services.Interfaces
{
    public interface IAccessClientHub
    {
        Task<HubConnection> WebSocketConnectionBuilderAsync(string socketUrl);
    }
}